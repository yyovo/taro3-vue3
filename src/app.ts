import { createApp } from "vue";
import { setupStore } from "./store";
import { setupAssets } from "./plugins";
import Taro from "@tarojs/taro";

const App = createApp({
	onLaunch(options) {
		// 程序入口 onLaunch 时调用
		// console.info("程序入口 onLaunch 时调用");
		// 新版本信息
		const updateManager = Taro.getUpdateManager();
		updateManager.onCheckForUpdate(function (res) {
			// 请求完新版本信息的回调
			// console.log("版本检测", res.hasUpdate);
		});
		updateManager.onUpdateReady(function () {
			Taro.showModal({
				title: "更新提示",
				content: "新版本已经准备好，是否重启应用？",
				success: function (res) {
					if (res.confirm) {
						// 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
						updateManager.applyUpdate();
					}
				},
			});
		});
		updateManager.onUpdateFailed(function () {
			console.log("新的版本下载失败");
		});
	},
	onShow(options) {
		// 程序显示 / 隐藏时调用
		// console.info("程序入口 onShow");
	},
	// 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
});

function setupApp() {
	/** 引入静态资源 */
	setupAssets();

	/** 挂载store */
	setupStore(App);
}

setupApp();

export default App;
