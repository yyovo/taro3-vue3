import { request } from '../request';

interface Demo {
  name: string;
}

/** 示例 */
export function fetchDemo(data: any) {
  return request.post<Demo>('/route/to/demo', data, {
    useErrMsg: true
  });
}

export function fetchApi(data: any) {
  return request.post<T>('/data/test/fetch', data, {
    useErrMsg: true
  });
}

export function getConf(id: any) {
  return request.get<T>(`/system/configure/get?id=${id}`, null, {
    useErrMsg: true
  });
}

export function listVideo(data: any) {
  return request.post<T>('/data/test/videoList', data, {
    useErrMsg: true
  });
}

export function listLog(page: Number, size: Number) {
  return request.post<T>(`/system/log/oper/list?current=${page}&size=${size}`, null, {
    useErrMsg: true
  });
}

export function wxLogin(data: any) {
  return request.post<T>(`/data/wx/login`, data, {
    useErrMsg: true
  });
}