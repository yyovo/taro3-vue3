import { getEnv, getAccountInfoSync, showToast } from '@tarojs/taro';
import { HOSTS, CONTENT_TYPE, ERROR_MSG_DURATION } from '@/constants';
import { useAuthStore } from '@/store';
import { useRouterPush } from '@/composables';
import { localStg, exeStrategyActions } from '@/utils';

const env = getEnv();

/**
 * 获取请求路径
 * @param url
 */
export function getRequestUrl(url: string) {
  let baseUrl = '';
  const actions: Common.StrategyAction[] = [
    [env === 'WEB', () => (baseUrl = `/api${url}`)],
    [
      env === 'WEAPP',
      () => {
        const { miniProgram } = getAccountInfoSync();
        baseUrl = url.substring(0, 1) === '/' ? `${HOSTS[miniProgram.envVersion]}${url}` : `${url}`;
      }
    ],
    [
      true,
      () => {
        baseUrl = url.substring(0, 1) === '/' ? `${process.env.HTTP_URL}${url}` : `${url}`;
      }
    ]
  ];
  exeStrategyActions(actions);
  return baseUrl;
}

/** 获取请求头 */
export function getRequestHeaders(axiosConfig: Service.AxiosConfig) {
  const header: TaroGeneral.IAnyObject = {};
  /** 获取token */
  const token = localStg.get('token');
  if (token) {
    /** 添加token */
    header.Authorization = token;
  }
  /** 增加类型 */
  header['Content-Type'] = axiosConfig.contentType || CONTENT_TYPE.json;
  return header;
}

/** token过期 */
export function handleExpireToken() {
  const { resetAuthStore } = useAuthStore();
  const { toLogin } = useRouterPush();
  resetAuthStore();
  toLogin();

  return null;
}

export function showErrorMsg(message: string) {
  showToast({
    title: message,
    icon: 'none',
    duration: ERROR_MSG_DURATION
  });
}

export function jsonToUrlParams(jsonStr: string): string {
  // 解析 JSON 字符串为 JavaScript 对象
  const jsonObj = JSON.parse(jsonStr);
  
  // 初始化一个空数组，用于存储 URL 参数字符串的各个部分
  const paramsArray: string[] = [];

  // 遍历对象的属性，构建 URL 参数字符串
  for (const key in jsonObj) {
      if (jsonObj.hasOwnProperty(key)) {
          const value = encodeURIComponent(jsonObj[key]);
          paramsArray.push(`${encodeURIComponent(key)}=${value}`);
      }
  }

  // 将数组中的各个部分连接起来，以 & 符号分隔
  return paramsArray.join('&');
}
