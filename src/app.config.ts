export default defineAppConfig({
	pages: [
		"pages/index/index",
		"pages/mine/index",
		"pages/kitchen/index",
		"pages/film/index",
		"pages/film/play",
		"pages/film/live",
		"pages/news/index",
		"pages/mine/memo",
		"pages/mine/fortune",
		"pages/mine/message"
	],
	window: {
		backgroundColor: "#fff",
		backgroundTextStyle: "light",
		navigationBarBackgroundColor: "#fff",
		navigationBarTitleText: "Taro3",
		navigationBarTextStyle: "black",
		navigationStyle: "custom",
	},
	subPackages: [
		{
			root: "package",
			pages: [
				"package-a/index",
				"package-c/index",
				"icon/index",
			],
		},
	],
	tabBar: {
		custom: true,
		color: "#000000",
		selectedColor: "#FF0000",
		list: [
			{
				pagePath: "pages/index/index",
				text: "首页",
			},
			{
				pagePath: "pages/kitchen/index",
				text: "御膳房",
			},
			{
				pagePath: "pages/mine/index",
				text: "个人中心",
			},
		],
	},
	lazyCodeLoading: "requiredComponents",
	// style: "v2",
	// renderer: "skyline",
	// rendererOptions: {
	// 	skyline: {
	// 		defaultDisplayBlock: false
	// 	}
	// },
	// componentFramework: "glass-easel"
});
