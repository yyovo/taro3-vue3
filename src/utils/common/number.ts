/**
 * 根据数字获取对应的汉字
 * @param num - 数字(0-10)
 */
export function getHanByNumber(num: number) {
  const HAN_STR = '零一二三四五六七八九十';
  return HAN_STR.charAt(num);
}

/**
 * 将总秒数转换成 分：秒
 * @param seconds - 秒
 */
export function transformToTimeCountDown(seconds: number) {
  const SECONDS_A_MINUTE = 60;
  function fillZero(num: number) {
    return num.toString().padStart(2, '0');
  }
  const minuteNum = Math.floor(seconds / SECONDS_A_MINUTE);
  const minute = fillZero(minuteNum);
  const second = fillZero(seconds - minuteNum * SECONDS_A_MINUTE);
  return `${minute}: ${second}`;
}

/**
 * 获取指定整数范围内的随机整数
 * @param start - 开始范围
 * @param end - 结束范围
 */
export function getRandomInterger(end: number, start = 0) {
  const range = end - start;
  const random = Math.floor(Math.random() * range + start);
  return random;
}

// 版本比较
export function version_compare(v1, v2) {
  //将两个版本号拆成数组
  var arr1 = v1.split('.'),
      arr2 = v2.split('.');
  var minLength=Math.min(arr1.length,arr2.length);
  //依次比较版本号每一位大小
  for (var i = 0; i < minLength; i++) {
      if (parseInt(arr1[i]) != parseInt(arr2[i])) {
          return (parseInt(arr1[i]) > parseInt(arr2[i])) ? 1 : -1;
      }
  }
  // 若前几位分隔相同，则按分隔数比较。
  if (arr1.length == arr2.length) {
      return 0;
  } else {
      return (arr1.length > arr2.length) ? 1 : -1;
  }
}
