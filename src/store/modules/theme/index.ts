import { defineStore } from 'pinia';

type ConfigProviderTheme = 'light' | 'dark';
interface ThemeStore {
  theme: ConfigProviderTheme;
  themeVars: object;
  localVersion: string;
  remoteVersion: string;
}

export const useThemeStore = defineStore('theme-store', {
  state: (): ThemeStore => ({
    theme: 'light',
    themeVars: {
      primaryColor: '#fa2c19',
      primaryColorEnd: '#fa6419',
      navbarPadding: '0 0',
      navbarTitleFontColor: '#000',
      navbarTitleFont: '18px',
      navbarTitleFontWeight: '500'
    },
    localVersion: '0',
    remoteVersion: '0',
  }),
  getters: {
    getTheme: state => state.theme,
    getThemeVars: state => state.themeVars
  },
  actions: {
    /** 设置系统主题 */
    setTheme(theme: ConfigProviderTheme) {
      this.theme = theme;
    },
    /** 设置系统主题颜色 */
    setThemeVars(vars: object) {
      Object.assign(this.themeVars, vars);
    }
  }
});
